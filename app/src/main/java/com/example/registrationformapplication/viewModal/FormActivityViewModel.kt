package com.example.registrationformapplication.viewModal

import android.app.DatePickerDialog
import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.registrationformapplication.model.Student
import com.example.registrationformapplication.obserable.FormFieldObserable
import com.example.registrationformapplication.repository.SqliteDBRepository
import java.util.Calendar

@Suppress("NAME_SHADOWING")
class FormActivityViewModel(private val repository : SqliteDBRepository,private val context:Context) : ViewModel() {
    //----------------------------------------------------------------------------------------------
    private var firstName:String=""
    private var lastName:String=""
    private var mobileNumber:String=""
    private var altMobileNumber:String=""
    private var email:String=""
    private var _gender:String=""
    private var dob:String=""
    private var address:String=""
    private lateinit var languages:String

    //----------------------------------------------------------------------------------------------
    val formfieldobserable : FormFieldObserable = FormFieldObserable()


    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    fun datavalidation(gender : String, arrayList : ArrayList<String>,base64Image:String):Boolean {
        firstName= formfieldobserable.fnameform
        lastName= formfieldobserable.lnameform
        mobileNumber= formfieldobserable.mobilenumberform
        altMobileNumber= formfieldobserable.altnumberform
        address= formfieldobserable.addressform
        email= formfieldobserable.emailform
        dob=formfieldobserable.dobform
        this._gender = gender
        languages= arrayList.toString()
        if (firstName.isNotEmpty() && lastName.isNotEmpty() && mobileNumber.isNotEmpty() && altMobileNumber.isNotEmpty() && address.isNotEmpty() && email.isNotEmpty() && dob.isNotEmpty() &&gender.isNotEmpty() && languages.isNotEmpty()){
            return repository.createData(firstName,lastName,mobileNumber,altMobileNumber,dob,email,_gender,address,languages,base64Image)
        }
        return false

    }
    //----------------------------------------------------------------------------------------------
    fun onClickDob(){
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        // Create a DatePickerDialog to allow the user to select a date
        val datePickerDialog = DatePickerDialog(context, { _,year, month, dayOfMonth ->
            // Update the editText view with the selected date
            val selectedDate = "$dayOfMonth/${month+1}/$year"
            formfieldobserable.dobform=selectedDate
        }, year, month, day)

        // Show the DatePickerDialog
        datePickerDialog.show()
    }

    fun getAllData(): ArrayList<Student>{
       return repository.getAllData()
    }


    fun deleteSingleStudentRecord(rowId:Int):Boolean{
        return repository.deleteSingleRecord(rowId)
    }

    fun updateSingleStudentRecord(firstName:String,lastName:String,phoneNumber:String,rowId:Int):Boolean{
        return repository.updateSingleRecord(firstName,lastName,phoneNumber,rowId)
    }

    fun deleteAllRecord():Boolean{
        return repository.deleteAllRecords()
    }

}
//--------------------------------------------------------------------------------------------------